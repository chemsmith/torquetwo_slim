<?php

$config = [
    'settings' => [
        'displayErrorDetails' => true,
        'timezone' => 'Australia/Melbourne',
        'displayPromoModal' => false,
    ],
];