<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Dflydev\FigCookies\FigRequestCookies;

require_once '../vendor/autoload.php';
(new Dotenv\Dotenv('../config/'))->load();
require_once '../config/config.php';

date_default_timezone_set($config['settings']['timezone']);

$app = new \Slim\App($config);

session_start();

$app->add(function (Request $request, Response $response, $next) {
    $wanted_cookies = array('banner_promo');
    $cookies = array();
    foreach ($wanted_cookies as $cookie) {
        $full_cookie = FigRequestCookies::get($request, 'banner_promo', '')->getValue();
        $cookies[$cookie] = substr($full_cookie, strpos($full_cookie, '='));
    }
    $request = $request->withAttribute('cookies', $cookies);
    return $next($request, $response);
});

$container = $app->getContainer();

$app->add(function (Request $request, Response $response, $next) use ($config) {
    if (!empty($config['displayPromoModal'])) {
        $display_promo = isset($_SESSION['display_promo']) ? $_SESSION['display_promo'] : 'true';
        $request = $request->withAttribute('display_promo', $display_promo);
        $_SESSION['display_promo'] = 'false';
    }
    return $next($request, $response);
});

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../app/templates', [
        'cache' => false
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
    $view->getEnvironment()->addGlobal("current_path", $container["request"]->getUri()->getPath());

    return $view;
};

$app->get('/', function (Request $request, Response $response) {
    return $this->view->render($response, 'home.twig', [
        'title' => 'Fitness, training, and performance centre | Torque Movement',
        'banner_promo' => $request->getAttribute('cookies')['banner_promo'],
        'modal_promo' => $request->getAttribute('display_promo')
    ]);
});

$app->get('/about', function (Request $request, Response $response) {
    return $this->view->render($response, 'about.twig', [
        'title' => 'About | Torque Movement',
        'banner_promo' => $request->getAttribute('cookies')['banner_promo'],
        'modal_promo' => $request->getAttribute('display_promo')
    ]);
});

$app->get('/classes', function (Request $request, Response $response) {
    return $this->view->render($response, 'classes.twig', [
        'title' => 'Classes | Torque Movement',
        'banner_promo' => $request->getAttribute('cookies')['banner_promo'],
        'modal_promo' => $request->getAttribute('display_promo')
    ]);
});

$app->get('/book/classes', function (Request $request, Response $response) {
    return $this->view->render($response, 'bookclasses.twig', [
        'title' => 'Book Classes | Torque Movement',
        'banner_promo' => $request->getAttribute('cookies')['banner_promo'],
        'modal_promo' => $request->getAttribute('display_promo')
    ]);
});

$app->get('/book/intensity', function (Request $request, Response $response) {
    return $this->view->render($response, 'bookintensity.twig', [
        'title' => 'Book Intensity Classes | Torque Movement',
        'banner_promo' => $request->getAttribute('cookies')['banner_promo'],
        'modal_promo' => $request->getAttribute('display_promo')
    ]);
});

$app->get('/book/open_space', function (Request $request, Response $response) {
    return $this->view->render($response, 'bookopen.twig', [
        'title' => 'Book Open Space | Torque Movement',
        'banner_promo' => $request->getAttribute('cookies')['banner_promo'],
        'modal_promo' => $request->getAttribute('display_promo')
    ]);
});

$app->get('/training', function (Request $request, Response $response) {
    return $this->view->render($response, 'training.twig', [
        'title' => 'Personal Training | Torque Movement',
        'banner_promo' => $request->getAttribute('cookies')['banner_promo'],
        'modal_promo' => $request->getAttribute('display_promo')
    ]);
});

$app->run();
?>

