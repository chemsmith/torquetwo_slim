function fa() {
    var i = setInterval(function () {
        if (document.getElementById('instafeed').firstElementChild) {
            clearInterval(i);
            var cnt = $("#instafeed").contents();
            $("#instafeed").replaceWith(cnt);
            $("div.insta-cols:nth-child(4)").addClass("d-none d-md-block");
            $("div.insta-cols:nth-child(5)").addClass("d-none d-md-block");
            $("div.insta-cols:nth-child(6)").addClass("d-none d-md-block");
        }
    }, 100);
}

var userFeed = new Instafeed({
    get: 'user',
    accessToken: '2242239349.1677ed0.038da6d5eddf4f0c8b3cc4effa3776e3',
    userId: '2242239349',
    limit: '6',
    resolution: 'standard_resolution',
    after: fa(),
    filter: function (image) {
        image.create_time_ago = moment.unix(image.created_time).fromNow();

        if (image.type == 'image') {
            image.template = '<img class="card-img-top img-fluid" src="' + image.images.standard_resolution.url + '" />';
        } else {
            image.template = '<video class="embed-responsive card-img-top" onclick="this.paused ? this.play() : this.pause();" loop><source src="' + image.videos.standard_resolution.url + '" type="video/mp4"/></video>';
        }
        return true;
    },
    template: '<div class="col-12 col-lg-4 insta-cols"> \
                <div class="card mx-auto"> \
                  <a data-toggle="collapse" href="#instagram_{{id}}" aria-expanded="false" aria-controls="instagram_{{id}}"> \
                    {{model.template}} \
                  </a> \
                  <div class="collapse" id="instagram_{{id}}"> \
                    <div class="card-body"> \
                      <p class="card-text">{{caption}}</p> \
                      <p class="card-text"><small class="text-muted">Added {{model.create_time_ago}}</small></p> \
                    </div> \
                  </div> \
                </div> \
              </div>'
});
userFeed.run();